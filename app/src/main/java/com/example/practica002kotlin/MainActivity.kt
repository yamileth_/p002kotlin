package com.example.practica002kotlin

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.Toast

class MainActivity : AppCompatActivity() {

    private lateinit var btnCalcular: Button;
    private lateinit var btnLimpiar: Button;
    private lateinit var btnRegresar: Button;
    private lateinit var txtPeso: EditText;
    private lateinit var txtAltura: EditText;
    private lateinit var txtResultado: EditText;

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        btnCalcular = findViewById(R.id.btnCalcular)
        btnLimpiar = findViewById(R.id.btnLimpiar)
        btnRegresar = findViewById(R.id.btnRegresar)
        txtPeso = findViewById(R.id.txtPeso)
        txtAltura = findViewById(R.id.txtAltura)
        txtResultado = findViewById(R.id.txtResultado)

        btnCalcular.setOnClickListener {
            if (txtAltura.text.toString().isEmpty() || txtPeso.text.toString().isEmpty()) {
                Toast.makeText(this, "Por favor, complete todos los campos.", Toast.LENGTH_SHORT).show()
            } else {
                val altura = txtAltura.text.toString().toFloat() // Capturar altura en metros
                val peso = txtPeso.text.toString().toFloat()

                val imc = peso / (altura * altura) // Calcular IMC

                val resultado = String.format("%.2f", imc)

                txtResultado.setText(resultado)
            }
        }

        btnLimpiar.setOnClickListener {
            txtAltura.setText("")
            txtPeso.setText("")
            txtResultado.setText("")
            txtAltura.requestFocus()
            txtPeso.requestFocus()
        }

        btnRegresar.setOnClickListener {
            finish()
        }
    }
}